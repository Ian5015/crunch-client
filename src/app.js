require('angular');
var Crunch;
(function (Crunch) {
    'use strict';
    Crunch.app = angular.module('crunch', ['crunch.home']);
    console.log("test");
})(Crunch || (Crunch = {}));
var Blocks;
(function (Blocks) {
    var Exception;
    (function (Exception) {
        'use strict';
        angular
            .module('blocks.exception', ['blocks.log']);
    })(Exception = Blocks.Exception || (Blocks.Exception = {}));
})(Blocks || (Blocks = {}));
var Blocks;
(function (Blocks) {
    var Log;
    (function (Log) {
        'use strict';
        angular.module('blocks.log', []);
    })(Log = Blocks.Log || (Blocks.Log = {}));
})(Blocks || (Blocks = {}));
var Blocks;
(function (Blocks) {
    var Router;
    (function (Router) {
        'use strict';
        require('angular-ui-router');
        angular.module('blocks.router', [
            'ui.router',
            'blocks.log'
        ]);
    })(Router = Blocks.Router || (Blocks.Router = {}));
})(Blocks || (Blocks = {}));
var Crunch;
(function (Crunch) {
    var Core;
    (function (Core) {
        'use strict';
        angular.module('crunch.core', []);
    })(Core = Crunch.Core || (Crunch.Core = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Home;
    (function (Home) {
        'use strict';
        angular.module('crunch.home', []);
    })(Home = Crunch.Home || (Crunch.Home = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Profile;
    (function (Profile) {
        'use strict';
        angular.module('crunch.profile', []);
    })(Profile = Crunch.Profile || (Crunch.Profile = {}));
})(Crunch || (Crunch = {}));
var Blocks;
(function (Blocks) {
    var Exception;
    (function (Exception) {
        'use strict';
        var ExceptionHandlerProvider = (function () {
            function ExceptionHandlerProvider() {
                this.config = {
                    appErrorPrefix: ''
                };
            }
            ExceptionHandlerProvider.prototype.configure = function (appErrorPrefix) {
                this.config.appErrorPrefix = appErrorPrefix;
            };
            ExceptionHandlerProvider.prototype.$get = function () {
                return { config: this.config };
            };
            return ExceptionHandlerProvider;
        })();
        config.$inject = ['$provide'];
        function config($provide) {
            $provide.decorator('$exceptionHandler', extendExceptionHandler);
        }
        extendExceptionHandler.$inject = ['$delegate', 'exceptionHandler', 'logger'];
        function extendExceptionHandler($delegate, exceptionHandler, logger) {
            return function (exception, cause) {
                var appErrorPrefix = exceptionHandler.config.appErrorPrefix || '';
                var errorData = { exception: exception, cause: cause };
                exception.message = appErrorPrefix + exception.message;
                $delegate(exception, cause);
                logger.error(exception.message, errorData, '');
            };
        }
        angular
            .module('blocks.exception')
            .provider('exceptionHandler', ExceptionHandlerProvider)
            .config(config);
    })(Exception = Blocks.Exception || (Blocks.Exception = {}));
})(Blocks || (Blocks = {}));
var Blocks;
(function (Blocks) {
    var Exception;
    (function (Exception_1) {
        'use strict';
        var Exception = (function () {
            function Exception(logger) {
                this.logger = logger;
            }
            Exception.prototype.catcher = function (message) {
                var _this = this;
                return function (reason) { return _this.logger.error(message, reason, ''); };
            };
            Exception.$inject = ['logger'];
            return Exception;
        })();
        Exception_1.Exception = Exception;
        angular
            .module('blocks.exception')
            .factory('exception', Exception);
    })(Exception = Blocks.Exception || (Blocks.Exception = {}));
})(Blocks || (Blocks = {}));
var Blocks;
(function (Blocks) {
    var Log;
    (function (Log) {
        var Logger = (function () {
            function Logger($log, toastr) {
                this.$log = $log;
                this.toastr = toastr;
            }
            Logger.prototype.log = function () {
                return this.$log;
            };
            Logger.prototype.error = function (message, data, title) {
                this.toastr.error(message, title);
                this.$log.error(message, data);
            };
            Logger.prototype.info = function (message, data, title) {
                this.toastr.info(message, title);
                this.$log.info(message, data);
            };
            Logger.prototype.success = function (message, data, title) {
                this.toastr.success(message, title);
                this.$log.info(message, data);
            };
            Logger.prototype.warning = function (message, data, title) {
                this.toastr.warning(message, title);
                this.$log.warn(message, data);
            };
            Logger.$inject = ['$log', 'toastr'];
            return Logger;
        })();
        Log.Logger = Logger;
        angular
            .module('blocks.log')
            .service('logger', Logger);
    })(Log = Blocks.Log || (Blocks.Log = {}));
})(Blocks || (Blocks = {}));
var Blocks;
(function (Blocks) {
    var Router;
    (function (Router) {
        'use strict';
        var RouterHelperProvider = (function () {
            function RouterHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
                this.$locationProvider = $locationProvider;
                this.$stateProvider = $stateProvider;
                this.$urlRouterProvider = $urlRouterProvider;
                this.config = {
                    docTitle: '',
                    resolveAlways: {}
                };
                this.$locationProvider.html5Mode(false);
                this.$get.$inject = ['$location', '$rootScope', '$state', 'logger'];
            }
            RouterHelperProvider.prototype.configure = function (cfg) {
                angular.extend(this.config, cfg);
            };
            RouterHelperProvider.prototype.$get = function ($location, $rootScope, $state, logger) {
                var $stateProvider = this.$stateProvider;
                var $locationProvider = this.$locationProvider;
                var $urlRouterProvider = this.$urlRouterProvider;
                var config = this.config;
                var handlingStateChangeError = false;
                var hasOtherwise = false;
                var stateCounts = {
                    errors: 0,
                    changes: 0
                };
                var service = {
                    configureStates: configureStates,
                    getStates: getStates,
                    stateCounts: stateCounts
                };
                init();
                return service;
                function init() {
                    handleRoutingErrors();
                    updateDocTitle();
                }
                function configureStates(states, otherwiseState) {
                    $urlRouterProvider.when('', '/');
                    states.forEach(function (state) {
                        state.config.resolve =
                            angular.extend(state.config.resolve || {}, config.resolveAlways);
                        $stateProvider.state(state.state, state.config);
                    });
                    if (otherwiseState && !hasOtherwise) {
                        hasOtherwise = true;
                        $urlRouterProvider.otherwise(function ($injector, $location) {
                            var href = $state.href(otherwiseState, {
                                origin: $location.$$url
                            });
                            if (href.indexOf('#') === 0) {
                                return href.substr(1, href.length);
                            }
                            else {
                                return href;
                            }
                        });
                    }
                }
                function handleRoutingErrors() {
                    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
                        if (handlingStateChangeError) {
                            return;
                        }
                        stateCounts.errors++;
                        handlingStateChangeError = true;
                        var destination = (toState &&
                            (toState.title || toState.name || toState.loadedTemplateUrl)) ||
                            'unknown target';
                        var msg = 'Error routing to ' + destination + '. ' +
                            (error.data || '') + '. <br/>' + (error.statusText || '') +
                            ': ' + (error.status || '');
                        logger.warning(msg, [toState], '');
                        $location.path('/');
                    });
                }
                function getStates() {
                    return $state.get();
                }
                function updateDocTitle() {
                    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                        stateCounts.changes++;
                        handlingStateChangeError = false;
                        var title = config.docTitle + ' ' + (toState.title || '');
                        $rootScope.title = title;
                    });
                }
            };
            RouterHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
            return RouterHelperProvider;
        })();
        Router.RouterHelperProvider = RouterHelperProvider;
        angular
            .module('blocks.router')
            .provider('routerHelper', RouterHelperProvider);
        console.log('test 2');
    })(Router = Blocks.Router || (Blocks.Router = {}));
})(Blocks || (Blocks = {}));
var Crunch;
(function (Crunch) {
    var Core;
    (function (Core) {
        'use strict';
        var core = angular.module('crunch.core');
        var config = {
            appErrorPrefix: '[crunch Error] ',
            appTitle: 'crunch'
        };
        core.value('config', config);
    })(Core = Crunch.Core || (Crunch.Core = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Core;
    (function (Core) {
        'use strict';
        var core = angular.module('crunch.core');
        var moment = require('moment');
        var toastr = require('toastr');
        core.constant('moment', moment);
        core.constant('toastr', toastr);
    })(Core = Crunch.Core || (Crunch.Core = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Core;
    (function (Core) {
        'use strict';
        var core = angular.module('crunch.core');
        core.run(appRun);
        function appRun(routerHelper) {
            var otherwise = '404';
            routerHelper.configureStates(getStates(), otherwise);
        }
        function getStates() {
            return [
                {
                    state: '404',
                    config: {
                        url: '/404:origin',
                        templateUrl: 'app/core/notFound.html',
                        title: '404',
                        controller: 'notFoundController',
                        controllerAs: 'vm'
                    }
                }
            ];
        }
    })(Core = Crunch.Core || (Crunch.Core = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Core;
    (function (Core) {
        'use strict';
        var home = angular.module('crunch.core');
        var NotFoundController = (function () {
            function NotFoundController($state, logger) {
                this.origin = $state.params.origin;
                logger.warning('Unknown route', decodeURIComponent(this.origin), '404 - Not Found');
            }
            NotFoundController.$inject = ['$state', 'logger'];
            return NotFoundController;
        })();
        Core.NotFoundController = NotFoundController;
        home.controller('notFoundController', NotFoundController);
    })(Core = Crunch.Core || (Crunch.Core = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Home;
    (function (Home) {
        'use strict';
        var home = angular.module('crunch.home');
        var HomeController = (function () {
            function HomeController(moment) {
                this.msg = 'Welcome home to Crunch! It is: ' + moment().format('LLLL');
            }
            HomeController.$inject = ['moment'];
            return HomeController;
        })();
        Home.HomeController = HomeController;
        home.controller('homeController', HomeController);
    })(Home = Crunch.Home || (Crunch.Home = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Home;
    (function (Home) {
        'use strict';
        var home = angular.module('crunch.home');
        home.run(appRun);
        function appRun(routerHelper) {
            console.log('WHAT?! home');
            routerHelper.configureStates(getStates());
        }
        function getStates() {
            return [
                {
                    state: 'home',
                    config: {
                        url: '/',
                        templateUrl: 'app/home/home.html',
                        title: 'Home',
                        controller: 'homeController',
                        controllerAs: 'vm'
                    }
                }
            ];
        }
    })(Home = Crunch.Home || (Crunch.Home = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Profile;
    (function (Profile) {
        var profile = angular.module('crunch.profile');
        var ProfileController = (function () {
            function ProfileController() {
            }
            return ProfileController;
        })();
        Profile.ProfileController = ProfileController;
        profile.controller('profileController', ProfileController);
    })(Profile = Crunch.Profile || (Crunch.Profile = {}));
})(Crunch || (Crunch = {}));
var Crunch;
(function (Crunch) {
    var Profile;
    (function (Profile) {
        'use strict';
        var profile = angular.module('crunch.profile');
        profile.run(appRun);
        function appRun(routerHelper) {
            console.log('WHAT?!');
            routerHelper.configureStates(getStates());
        }
        function getStates() {
            return [
                {
                    state: 'profile',
                    config: {
                        url: '/',
                        templateUrl: 'app/profile/profile.html',
                        title: 'Profile',
                        controller: 'profileController',
                        controllerAs: 'vm'
                    }
                }
            ];
        }
    })(Profile = Crunch.Profile || (Crunch.Profile = {}));
})(Crunch || (Crunch = {}));
//# sourceMappingURL=app.js.map