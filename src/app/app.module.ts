declare var require:any;

require('angular');

module Crunch {
    'use strict';

    export var app: angular.IModule =
        angular.module('crunch', ['crunch.home']);
}
