module Crunch.Home {
    'use strict';

    angular.module('crunch.home', ['blocks.exception', 'blocks.log', 'blocks.router', 'crunch.core']);
}
