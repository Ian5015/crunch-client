module Crunch.Home {
    'use strict';

    // var moment = require('moment');
    var home = angular.module('crunch.home');
    // home.constant('moment', moment);

    export class HomeController {
        static $inject = ['moment'];

        public msg : string;

        constructor(moment : moment.MomentStatic) {
           this.msg = 'Welcome home to Crunch! It is: ' + moment().format('LLLL');
        }
    }

    home.controller('homeController', HomeController);
}
