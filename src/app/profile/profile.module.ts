module Crunch.Profile {
  'use strict';

  angular.module('crunch.profile', ['blocks.exception', 'blocks.log', 'blocks.router',]);
}
