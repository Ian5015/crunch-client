module Crunch.Profile {
    'use strict';

    var profile = angular.module('crunch.profile');

    profile.run(appRun);

    function appRun(routerHelper : Blocks.Router.IRouterHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'profile',
                config: {
                    url: '/',
                    templateUrl: 'app/profile/profile.html',
                    title: 'Profile',
                    controller: 'profileController',
                    controllerAs: 'vm'
                }
            }
        ];
    }
}
