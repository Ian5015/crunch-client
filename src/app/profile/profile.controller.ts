module Crunch.Profile {

  var profile = angular.module('crunch.profile');

  export class ProfileController {
    // static $inject = [];

    public msg: string;
  }

  profile.controller('profileController', ProfileController);
}
