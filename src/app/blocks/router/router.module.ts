module Blocks.Router {
    'use strict';

    require('angular-ui-router');

    angular.module('blocks.router', [
        'ui.router',
        'blocks.log'
    ]);
}
