module Crunch.Core {
    'use strict';

    var core = angular.module('crunch.core');

    var config = {
        appErrorPrefix: '[crunch Error] ',
        appTitle: 'crunch'
    };

    core.value('config', config);
}
