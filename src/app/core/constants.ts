module Crunch.Core {
    'use strict';

    var core = angular.module('crunch.core');
    var moment = require('moment');
    var toastr = require('toastr');

    core.constant('moment', moment);
    core.constant('toastr', toastr);
}
