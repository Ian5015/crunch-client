module Crunch.Core {
    'use strict';
    angular.module('crunch.core', ['blocks.exception', 'blocks.log', 'blocks.router',]);
}
